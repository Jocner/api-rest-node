'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 3977;

mongoose.set('useNewUrlParser', true);
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/microservicio', (err, res) => {
    if(err){
        throw err;
    }else{
        console.log("La conexion a la base de datos esta funcionando correctamente..!!!");

        app.listen(port, function(){
            console.log("Servidor de la api rest de musica esta escuchando en http://localhost:"+ port);
        });
    }
});