'use strict'

var validator = require('validator');
var Article = require('../models/article');


var controller = {

    save: function (req, res) {

        //Recoger parametros por post
        var params = req.body;

        //Validar datos
        try {
            var validate_name = !validator.isEmpty(params.name);
            var validate_description = !validator.isEmpty(params.description);
            var validate_time = !validator.isEmpty(params.time);

        }catch (err) {
            return res.status(200).send({
                message: 'Faltan datos por enviar'
            });
        }

        if (validate_name && validate_description && validate_time){

                //Crear objeto a guardar
                var article = new Article();

                //Asignar valores
                article.name = params.name;
                article.description = params.description;
                article.time = params.time;

                //Guardar el article
                article.save((err, articleStored) => {
                    console.log(articleStored);
                    if (err || !articleStored){
                        return res.status(404).send({
                            status: 'error',
                            message: 'Error al guardar articulo'
                        });
                    }

                    //Devolver una respuesta
                    return res.status(200).send({
                        status:'success',
                        article: articleStored
                    });
                });


        }else{
            return res.status(200).send({
                message: 'Los datos no son validos'
            });
        }

    },

    getArticle: function(req, res) {
        Article.find().sort('time').exec((err, articles) => {
            if(err || !articles){
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay articulos'
                });
            }

            return res.status(200).send({
                status: 'success',
                articles
            });
        });
    },

    deleteArticle: function(req, res){

        var articleId = req.params.id;
        console.log(articleId);

        Article.findByIdAndDelete(articleId, (err, articleDeleted) => {
            if(err){
                res.status(500).send({
                    message: 'Error en la peticion'
                });  
            }else{
                if(!articleDeleted){
                    console.log(articleDeleted);
                    res.status(404).send({
                        message: 'Error el articulo no se ha borrado'
                    });  
                }else{
                    res.status(200).send({
                        status:'success',
                        articleDeleted
                    });   
                }
            }
        });

    }

};

module.exports = controller;